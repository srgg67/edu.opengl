const double	W = 500;
const double	H = 350;

const GLdouble	unitsHeight = 1.5;
const GLdouble	unitsWidth = unitsHeight * W / H;

const GLfloat	Radius	= 1.0;
const GLfloat	Scale		= 1.0;
const GLfloat	Angle		= 0.0;

GLfloat xPos, XPos, yPos, YPos, radius, angle, scale;

const int		Stacks	= 10;
const int		Slices	= 20;

GLint stacks, slices;
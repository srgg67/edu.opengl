/*	Education C++ OpenGL OOP gluOrtho2D glViewport gluSphere glTranslate* glRotatef */
#include "windows.h"
#include <GL/glut.h>
#include "api.h"
#include "funx.h"
#include <iostream>
using namespace std;

void Reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	GLdouble offsetLeft		= -unitsWidth;
	GLdouble offsetRight	= unitsWidth;
	GLdouble offsetBottom	= -unitsHeight;
	GLdouble offsetTop		= unitsHeight;
	
	float default_ratio = W / H;

	GLdouble ratio_corrector = (float(w / h) <= default_ratio) ?
		GLdouble((float)h / (float)w * default_ratio)
		: GLdouble((float)w / (float)h * default_ratio);

	if (float(w / h) <= default_ratio)
	{
		offsetBottom *= ratio_corrector;
		offsetTop	 *= ratio_corrector;	//cout << "1. w/h <= default_ratio" << endl;
	}
	else
	{
		offsetLeft	 *= ratio_corrector;
		offsetRight  *= ratio_corrector;	//cout << "2. w/h > default_ratio" << endl;
	}	//cout << "w = " << w << ", h = " << h << "; default_ratio = "<< default_ratio << endl << "offsetLeft=" << offsetLeft << ", offsetRight=" << offsetRight << ", offsetBottom=" << offsetBottom << ", offsetTop=" << offsetTop << endl;
	gluOrtho2D(offsetLeft, offsetRight, offsetBottom, offsetTop);
}

void keyboard(unsigned char key, int x, int y)
{
	float stepRadius	= 0.05;
	int stepSlices		= 1;
	int stepStacks		= 1;
	float stepX			= 0.1;
	float stepY			= 0.1;
	float stepAngle		= 3.0;

	switch (key) 
	{
		case 43: // +
			radius += stepRadius;
			break;
		case 45: // -
			radius -= stepRadius;
			break;
		case 60: // < 
			slices -= stepSlices;
			break;
		case 62: // >
			slices += stepSlices;
			break;
		case 55: // 7 home
			stacks -= stepStacks;
			break;
		case 57: // 9 pgup
			stacks += stepStacks;
			break;
		case 52: // arrow left
			xPos -= stepX;
			break;
		case 54: // arrow right
			xPos += stepX;
			break;
		case 50: // arrow top
			yPos -= stepY;
			break;
		case 56: // arrow bottom
			yPos += stepY;
			break;
		case 49: // 1
			angle -= stepAngle;
			break;
		case 51: // 3
			angle += stepAngle;
			break;

		case 27: // Escape
			radius	= Radius;
			angle	= Angle;
			scale	= Scale;
			xPos	= XPos;
			yPos	= YPos;
			slices	= Slices;
			stacks	= Stacks;
			break;
	}	
	// spacebar
	if (key == 32) exit(0);
	else
		glutPostRedisplay();
}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glTranslatef(xPos,yPos,0.0);
	glRotatef(angle, 0.0, 0.0, 1.0);

	GLUquadricObj* shape;
	shape = gluNewQuadric();
	gluQuadricDrawStyle(shape, GLU_FILL);
	gluSphere(shape,radius, slices, stacks);
	/*
	Funx mkFunx;
	mkFunx.ShowLog(radius,slices,stacks); */
	glFlush();
}

void init()
{
	glClearColor(0.8, 0.8, 1.0, 1.0);
	glColor3f(0.5, 0.0, 0.0);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluOrtho2D(-unitsWidth, unitsWidth, -unitsHeight, unitsHeight);
	cout <<"ratio: "<< W << " / " << H << " = " << (W / H) << "; unitsWidth: " << unitsWidth << ", unitsHeight: " << unitsHeight << endl;
	XPos	= 0.0;
	YPos	= 0.0;
	xPos	= XPos;
	yPos	= YPos;
	radius	= Radius;
	angle	= Angle;
	scale	= Scale;
	stacks	= Stacks;
	slices	= Slices;
}

void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(W,H);
	glutInitWindowPosition(700, 40);
	glutCreateWindow("Application name");
	init();
	setlocale(LC_ALL, "Russian");
	cout << "������� ����������: " <<
		endl << "�������� �� �����������/���������: 4, 6, 8, 2" <<
		endl << "������ �����: +/-" <<
		endl << "���������� ��������: </>" <<
		endl << "���������� ��������: 7/9" <<
		endl << "..............................................." <<
		endl << "��������� � ��������� �� ���������:\tEsc" << endl << endl;
	glutDisplayFunc(Display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(Reshape);
	glutMainLoop();
}

const double	W = 500;
const double	H = 500;

const GLdouble	unitsHeight = 1.5;
const GLdouble	unitsWidth = unitsHeight * W / H;

const float	Radius = 1.0;
const int	Stacks = 10;
const int	Slices = 20;

float	radius = Radius;
int		stacks = Stacks;
int		slises = Slices;
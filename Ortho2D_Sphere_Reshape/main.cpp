/*	Education C++ OpenGL Basics �������� ��������� ����� gluOrtho2D glViewport Reshape Slices Stacks
*/
#include "windows.h"
#include <GL/glut.h>
#include "api.h"
#include <iostream>
using namespace std;

// ��� ������������ �������������� ������� ��������:
GLfloat pLeft	= 0.0;
GLfloat pRight	= 0.0;
GLfloat pBottom = 0.0;
GLfloat pTop	= 0.0;

void ShowLog()
{
	setlocale(LC_ALL, "Russian");
	cout << "������ ����� (radius):\t\t" << radius << "\t[+/-]" << endl
		<< "���������� ������ (slises):\t" << slises << "\t[4/6]" << endl
		<< "���������� ������ (staks):\t" << stacks << "\t[7/9]" << endl << endl;
}
void Reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	GLdouble offsetLeft = -unitsWidth;
	GLdouble offsetRight = unitsWidth;
	GLdouble offsetBottom = -unitsHeight;
	GLdouble offsetTop = unitsHeight;

	float default_ratio = W / H;

	GLdouble ratio_corrector = (float(w / h) <= default_ratio) ?
		GLdouble((float)h / (float)w * default_ratio)
		: GLdouble((float)w / (float)h * default_ratio);

	if (float(w / h) <= default_ratio)
	{
		offsetBottom *= ratio_corrector;
		offsetTop *= ratio_corrector;	//cout << "1. w/h <= default_ratio" << endl;
	}
	else
	{
		offsetLeft *= ratio_corrector;
		offsetRight *= ratio_corrector;	//cout << "2. w/h > default_ratio" << endl;
	}	//cout << "w = " << w << ", h = " << h << "; default_ratio = "<< default_ratio << endl << "offsetLeft=" << offsetLeft << ", offsetRight=" << offsetRight << ", offsetBottom=" << offsetBottom << ", offsetTop=" << offsetTop << endl;
	gluOrtho2D(offsetLeft, offsetRight, offsetBottom, offsetTop);
}
void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 43: // +
		radius += 0.05;
		break;
	case 45: // -
		radius -= 0.05;
		break;
	case 52: // 4, arrow left
		slises -= 1;
		break;
	case 54: // 6, arrow right
		slises += 1;
		break;
	case 55: // 7 home
		stacks -= 1;
		break;
	case 57: // 9 pgup
		stacks += 1;
		break;
	case 27: // Escape
		radius = Radius;
		slises = Slices;
		stacks = Stacks;
		break;
	}
	// spacebar
	if (key == 32) 
		exit(0);
	else
		glutPostRedisplay();
}
void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.35, 0.35, 0.8);
	glBegin(GL_POLYGON);
		// ������, ����������� ������� ��������������� ��������:
		glVertex2f(pLeft, pBottom);		//
		glVertex2f(pRight, pBottom);	//
		glVertex2f(pRight, pTop);		//
		glVertex2f(pLeft, pTop);		//
	glEnd();
	
	bool show_edges = false;
	if (show_edges)
	{
		cout << "pLeft, pLeft = \t\t"	<< pLeft	<< ":" << pLeft		<< endl;
		cout << "pRight, pBottom = \t"	<< pRight	<< ":" << pBottom	<< endl;
		cout << "pRight, pTop = \t\t"	<< pRight	<< ":" << pTop		<< endl;
		cout << "pLeft, pTop = \t\t"	<< pLeft	<< ":" << pTop		<< endl;
	}

	glBegin(GL_POLYGON);
		// ������, ����������� ��������� ��������������� ��������:
		glColor3f(0.75, 0.375, 0.225);
		glVertex2f(-unitsWidth, -unitsHeight);
		glVertex2f(unitsWidth, -unitsHeight);
		glColor3f(1.0, 0.75, 0.6);
		glVertex2f(unitsWidth, unitsHeight);
		glVertex2f(-unitsWidth, unitsHeight);
	glEnd();
	glColor3f(1.0, 1.0, 1.0);
	GLUquadricObj* qobj = gluNewQuadric();
	gluQuadricDrawStyle(qobj, GLU_FILL);
	gluSphere(qobj, radius, slises, stacks);
	ShowLog();
	glFlush();
}
void init()
{
	glClearColor(0.15, 0.15, 0.15, 1.0);
	glColor3f(1.0, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	GLdouble unitsHeight = 1.5;
	GLdouble unitsWidth = unitsHeight * W / H;

	gluOrtho2D(-unitsWidth, unitsWidth, -unitsHeight, unitsHeight);
}
void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(W,H);
	glutInitWindowPosition(700, 40);
	glutCreateWindow("Simple shpere");
	init();
	glutDisplayFunc(Display);
	setlocale(LC_ALL, "Russian");
	cout << "��������� � ��������� �� ���������:\tEsc" << endl << endl;
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(Reshape);
	glutMainLoop();
}
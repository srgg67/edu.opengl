/*	Education C++ OpenGL Basics gluOrtho2D
*/
#include "windows.h"
#include <GL/glut.h>
#include <iostream>
using namespace std;

const float spaceUnit = 3;

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBegin(GL_QUADS);
		glVertex2f(-1.5,-1.5);
		glVertex2f(-1.5, 1.5);
		glVertex2f(1.5, 1.5);
		glVertex2f(1.5, -1.5);
	glEnd();
	glFlush();
}
void init()
{
	glClearColor(0.8, 0.8, 1.0, 1.0);
	glColor3f(1.0, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-spaceUnit, spaceUnit, -spaceUnit, spaceUnit);
}

void keyboard(unsigned char key, int x, int y)
{
	if (key != 32)
		glutPostRedisplay();
	else // spacebar on keyboard
		exit(0);
}

void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(700, 40);
	glutCreateWindow("Application name");
	glutDisplayFunc(Display);
	init();
	glutMainLoop();
}


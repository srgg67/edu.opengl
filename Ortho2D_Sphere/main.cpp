/*	Education C++ OpenGL Basics �������� ��������� ����� gluOrtho2D Slices Stacks
*/
#include "windows.h"
#include <GL/glut.h>
#include <iostream>
using namespace std;

const float	Radius = 1.0;
const int	Stacks = 10;
const int	Slices = 20;

float	radius = Radius;
int		stacks = Stacks;
int		slises = Slices;

void ShowLog()
{
	setlocale(LC_ALL, "Russian");
	cout<< "������ ����� (radius):\t\t"		<< radius << "\t[+/-]" << endl
		<< "���������� ������ (slises):\t"	<< slises << "\t[4/6]" << endl
		<< "���������� ������ (staks):\t"	<< stacks << "\t[7/9]" << endl << endl;
}
void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	GLUquadricObj* qobj = gluNewQuadric();
	gluQuadricDrawStyle(qobj, GLU_FILL);
	gluSphere(qobj, radius, slises, stacks);
	ShowLog();
	glFlush();
}
void init()
{
	glClearColor(0.8, 0.8, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1.5, 1.5, -1.5, 1.5);
}
void keyboard(unsigned char key, int x, int y)
{	
	switch (key) {
		case 43: // +
			radius += 0.05;
			break;
		case 45: // -
			radius -= 0.05;
			break;
		case 52: // 4, arrow left
			slises -= 1;
			break;
		case 54: // 6, arrow right
			slises += 1;
			break;
		case 55: // 7 home
			stacks -= 1;
			break;
		case 57: // 9 pgup
			stacks += 1;
			break;
		case 27: // Escape
			radius = Radius;
			slises = Slices;
			stacks = Stacks;
			break;
	}
	(key == 32) ?	exit(0) // spacebar on keyboard
					:	glutPostRedisplay();
}
void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(700, 40);
	glutCreateWindow("Simple shpere");
	init();
	glutDisplayFunc(Display);
	setlocale(LC_ALL, "Russian");
	cout <<"��������� � ��������� �� ���������:\tEsc" << endl << endl ;
	glutKeyboardFunc(keyboard);
	glutMainLoop();
}